#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


#define DEBUG 0 //Easy way to turn on and off debugging
#ifdef DEBUG
#define PR(x) printf("%s = %s\n", #x, x);
#endif
#define BOARD_SIZE 10 
#define PLAYER_X   1 //used for recursive calls
#define PLAYER_O   0 //used for recursive calls
#define DRAW       0 //minmax score
#define WIN_X   1 //minmax score
#define WIN_O  -1 //minmax score
#define X_MARK    'X'
#define O_MARK    'O'
#define EMPTY     ' '
#define LOCK_FILE "./tictoe.lock"
#define HORIZ_0 (board[0] != EMPTY && board[0] == board[1] && board[0] == board[2])
#define HORIZ_3 (board[3] != EMPTY && board[3] == board[4] && board[3] == board[5])
#define HORIZ_6 (board[6] != EMPTY && board[6] == board[7] && board[6] == board[8])
#define VERT_0 (board[0]  != EMPTY && board[0] == board[3] && board[0] == board[6])
#define VERT_1 (board[1]  != EMPTY && board[1] == board[4] && board[1] == board[7])
#define VERT_2 (board[2]  != EMPTY && board[2] == board[5] && board[2] == board[8])
#define DIAG_0 (board[0]  != EMPTY && board[0] == board[4] && board[0] == board[8])
#define DIAG_2 (board[2]  != EMPTY && board[2] == board[4] && board[2] == board[6])
#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)


//[[0,1,2],[3,4,5],[6,7,8]
//[0,3,6],[1,4,7],[2,5,8]
//[0,4,8],[2,4,6]


int space_open (int location, char board[]);
void print_moves (char board[]);
int num_valid_moves (char board[]);
void get_valid_moves(char board[], char moves[]);
int game_over (char board[]);
void initBoard (char board[]);
char get_winner (char board[]);
void print_board (char board[]);
int move (int position, int player, char board[]);
int unmove (int position, char board[]);
void perform_lock (int fd, int action, int lock_type, int whence, int start,
		   int len);

int score(char board[]);
int minmax(int player, int game_states[]);
int find_move (int player, int the_move, char board[]);
int get_cpu_move( char board[]);

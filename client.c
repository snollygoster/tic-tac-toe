#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "mysock.h"
#include "tictoe.h"

int
main (int argc, char *argv[])
{
  int i;
  int fd;
  int sockfd;
  static struct sockaddr_in server_addr;
  socklen_t len;
  char buf[MAXLINE];
  char board[BOARD_SIZE];
  initBoard ((char *) board);
  int cpu_move;
  int c, valid_move = 0;
  if (argc != 2)
    {
      char my_error[BUFSIZ];
      snprintf (my_error, sizeof (my_error), "Usage: %s <IP>", argv[0]);
      perror (my_error);
      exit (EXIT_FAILURE);
    }
  sockfd = Socket (AF_INET, SOCK_STREAM, 0);
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons (65201);
  len = sizeof (server_addr);
  printf ("Attempting to bind to IP:%s Port:%s\n", argv[1], argv[2]);
  if ((inet_pton (AF_INET, argv[1], &server_addr.sin_addr)) <= 0)
    {
      perror ("inet_pton");
      exit (EXIT_FAILURE);
    }
  Connect (sockfd, (SA *) & server_addr, len);
  memset (buf, 0x0, MAXLINE);
  do
    {
      print_board ((char *) board);
      printf ("Valid Moves:");
      print_moves ((char *) board);
      write (STDOUT_FILENO, " >", 3);
      do
	{
	  if ((c = read (STDIN_FILENO, buf, BUFSIZ)) > 0)
	    {
	      valid_move = move (atoi (buf), X_MARK, (char *) board);
	    }
	  write (STDOUT_FILENO, " >", 3);
	}
      while (!valid_move);
      write (sockfd, board, sizeof (board));
      if ((c = read (sockfd, buf, BUFSIZ)) > 0)
	strncpy ((char *) board, buf, c);	//copy board from client
      write (STDOUT_FILENO, buf, c);
    }
  while (!game_over ((char *) board));
  print_board ((char *) board);
  switch (get_winner ((char *) board))
    {
    case EMPTY:
      printf ("It's a Draw, find a real game to play!\n");
      break;
    default:
      printf ("Player:%c has won the game!\n", get_winner ((char *) board));
    }
  close (sockfd);

}

/*Author:Bill Turczyn
 * Program: Tic tac toe, using minmax (brute force)
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "tictoe.h"



void test_board_logic ();
int
main ()
{
  //test_board_logic(); //used for testing
  char board[BOARD_SIZE];
  int cpu_move;
  char buf[BUFSIZ];
  int c, valid_move = 0;
  initBoard ((char *) board);
  do
    {
      print_board ((char *) board);
      printf ("Valid Moves:");
      print_moves ((char *) board);
      write (STDOUT_FILENO, " >", 3);
      do
	{
	  if ((c = read (STDIN_FILENO, buf, BUFSIZ)) > 0)
	    {
	      valid_move = move (atoi (buf), X_MARK, (char *) board);
	    }
	  write (STDOUT_FILENO, " >", 3);
	}
      while (!valid_move);
      print_board ((char *) board);
      cpu_move = get_cpu_move ((char *) board);
      if (DEBUG)
	printf ("Best move for CPU:%d", cpu_move);
      move (cpu_move, PLAYER_X, (char *) board);
    }
  while (!game_over ((char *) board));
  print_board ((char *) board);
  switch (get_winner ((char *) board))
    {
    case EMPTY:
      printf ("It's a Draw, find a real game to play!\n");
      break;
    default:
      printf ("Player:%c has won the game!\n", get_winner ((char *) board));
    }

}

void
test_board_logic ()
{
  char board[BOARD_SIZE];
  char moves[BOARD_SIZE];
  initBoard ((char *) board);
  print_board ((char *) board);
  print_moves ((char *) board);
  /*test minmax score function for player X */
  move (0, PLAYER_X, (char *) board);
  move (1, PLAYER_O, (char *) board);
  move (8, PLAYER_X, (char *) board);
  move (5, PLAYER_O, (char *) board);
  move (6, PLAYER_X, (char *) board);
  move (3, PLAYER_O, (char *) board);
  move (7, PLAYER_X, (char *) board);
  print_board ((char *) board);
  printf ("Board score:%d\n", score ((char *) board));
  /*test minmax score function for player O */
  move (0, PLAYER_O, (char *) board);
  move (1, PLAYER_X, (char *) board);
  move (2, PLAYER_O, (char *) board);
  move (3, PLAYER_O, (char *) board);
  move (4, PLAYER_X, (char *) board);
  move (5, PLAYER_X, (char *) board);
  move (6, PLAYER_X, (char *) board);
  move (7, PLAYER_O, (char *) board);
  move (8, PLAYER_O, (char *) board);
  print_board ((char *) board);
  printf ("Board score:%d\n", score ((char *) board));
  printf ("PLAYER: %c WON GAME OVER:SCORE %d Game Over:%d\n",
	  get_winner ((char *) board), score ((char *) board),
	  game_over ((char *) board));

  initBoard ((char *) board);
  move (0, PLAYER_O, (char *) board);
  move (1, PLAYER_X, (char *) board);
  move (8, PLAYER_O, (char *) board);
  move (5, PLAYER_X, (char *) board);
  move (6, PLAYER_O, (char *) board);
  move (3, PLAYER_X, (char *) board);
  move (7, PLAYER_O, (char *) board);
  print_board ((char *) board);
  printf ("Board score:%d\n", score ((char *) board));
  /*test minmax score function for player O */
  initBoard ((char *) board);
  move (0, PLAYER_X, (char *) board);
  move (8, PLAYER_O, (char *) board);
  move (6, PLAYER_X, (char *) board);
  move (2, PLAYER_O, (char *) board);
  move (5, PLAYER_X, (char *) board);
  move (3, PLAYER_O, (char *) board);
  move (7, PLAYER_X, (char *) board);
  move (1, PLAYER_O, (char *) board);
  move (4, PLAYER_X, (char *) board);
  print_board ((char *) board);
  printf ("Board score:%d\n", score ((char *) board));
}

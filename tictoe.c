#include "tictoe.h"


int
space_open (int location, char board[])
{
  if (location > BOARD_SIZE - 1)
    {
      perror ("Invalid location");
      return -1;
    }
  if (board[location] != EMPTY)	//Space has a character
    return 1;
  else
    return 0;
}

int
move (int position, int player, char board[])
{
  if (DEBUG)
    {
      PR (board);
      print_board (board);
    }
  switch (space_open (position, (char *) board))
    {
    case -1:
      break;
    case 0:
      if (DEBUG)
	printf ("SPACE IS NOT OCCUPIED Position:%d, making move\n", position);
      board[position] = player == PLAYER_X ? X_MARK : O_MARK;
      return 1;
      break;
    case 1:
      printf ("SPACE IS OCCUPIED\n");
      return 0;
      break;
    default:
      break;
    }
}
void
print_moves (char board[])
{
  char t[BOARD_SIZE];
  int i;
  printf (" %c", '[');
  for (i = 0; i < BOARD_SIZE - 1; i++)
    {
      if (board[i] == X_MARK || board[i] == O_MARK)
	continue;		//X or O in position
      t[i] = i + '0';
      printf (" %c ", t[i]);
    }
  printf (" ]\n");

}

int
num_valid_moves (char board[])
{
  int moves = 0;
  int i;
  for (i = 0; i < BOARD_SIZE - 1; i++)
    {
      if (board[i] == X_MARK || board[i] == O_MARK)
	continue;		//X or O in position
      moves++;
    }

  return moves;
}

void
get_valid_moves (char board[], char moves[])
{
  char t[BOARD_SIZE];
  int i;
  for (i = 0; i < BOARD_SIZE - 1; i++)
    {
      if (board[i] == EMPTY)
	{
	  t[i] = i + '0';
	  continue;
	}
      else
	{
	  t[i] = EMPTY;
	}

    }
  t[BOARD_SIZE - 1] = '\0';
  strncpy (moves, t, sizeof (t));
}


int
game_over (char board[])
{
  if (HORIZ_0 || HORIZ_3 || HORIZ_6 ||
      VERT_0 || VERT_1 || VERT_2 || DIAG_0 || DIAG_2
      || num_valid_moves ((char *) board) == 0)
    {
      return 1;
    }
  else
    return 0;

}

char
get_winner (char board[])
{
  if (HORIZ_0)
    return board[0];
  else if (HORIZ_3)
    return board[3];
  else if (HORIZ_6)
    return board[6];
  else if (VERT_0)
    return board[0];
  else if (VERT_1)
    return board[1];
  else if (VERT_2)
    return board[2];
  else if (DIAG_0)
    return board[0];
  else if (DIAG_2)		//Ouch, hard bug to track down
    return board[2];
  return EMPTY;
}

void
initBoard (char board[])
{
  int i;
  for (i = 0; i < BOARD_SIZE - 1; i++)
    {
      board[i] = EMPTY;
      if (DEBUG)
	printf ("%d\n", i);
    }
  if (DEBUG)
    printf ("%d\n", BOARD_SIZE);
  board[BOARD_SIZE - 1] = '\0';
}

void
print_board (char board[])
{
  int i;
  for (i = 0; i < BOARD_SIZE - 1; i++)
    {
      if (i % 3 == 0)
	printf ("\n");
      printf ("|  %c  ", board[i]);
      if ((i + 1) % 3 == 0)
	printf ("|");
    }
  printf ("\n");
}

void
perform_lock (int fd, int action, int lock_type, int whence, int start,
	      int len)
{
  struct flock lock;
  lock.l_type = lock_type;
  lock.l_whence = whence;
  lock.l_start = start;
  lock.l_len = len;
  if ((fcntl (fd, action, &lock)) < 0)
    {
      perror ("FLOCK");
    }


}

int
score (char board[])
{
  switch (get_winner ((char *) board))
    {
    case EMPTY:
      return DRAW;
      break;
    case O_MARK:
      return WIN_O;
      break;
    case X_MARK:
      return WIN_X;
      break;
    default:
      break;
    }

}


int
unmove (int position, char board[])
{
  if (DEBUG)
    {
      PR (board);
      print_board (board);
    }
  switch (space_open (position, (char *) board))
    {
    case -1:
      break;
    case 0:
      if (DEBUG)
	printf
	  ("UNMOVE:SPACE IS NOT OCCUPIED Position:%d, no need to unmove\n",
	   position);
      break;
    case 1:
      if (DEBUG)
	printf ("UNMOVE:SPACE IS OCCUPIED....removing move:%d\n", position);
      board[position] = EMPTY;
      break;
    default:
      break;
    }
}


int
minmax (int player, int game_states[])
{
  if (player == PLAYER_X)
    {
      int min = 1;
      int i;
      for (i = 0; i < BOARD_SIZE - 1; i++)
	{
	  if (DEBUG)
	    printf ("VAL:%d\n", game_states[i]);
	  if (game_states[i] == WIN_O)
	    {
	      if (DEBUG)
		printf ("\nEND COMPUTER EARLY RETURN MIN:%d\n", WIN_O);
	      return WIN_O;	//return -1
	    }
	  if (game_states[i] >= -1 && game_states[i] <= 1)
	    {
	      //printf("%d\n", game_states[i]);
	      min = MIN (game_states[i], min);	//return 0
	    }

	}
      if (DEBUG)
	printf ("\nEND COMPUTER EARLY RETURN MIN:%d\n", min);
      return min;

    }
  else
    {
      int max = -1;
      int i;
      for (i = 0; i < BOARD_SIZE - 1; i++)
	{
	  if (DEBUG)
	    printf ("VAL:%d\n", game_states[i]);
	  if (game_states[i] == WIN_X)
	    {
	      if (DEBUG)
		printf ("\nEND PLAYER EARLY RETURN MIN:%d\n", WIN_X);
	      return WIN_X;	//return 1 
	    }
	  if (game_states[i] >= -1 && game_states[i] <= 1)
	    {
	      // printf("%d\n", game_states[i]);
	      max = MAX (game_states[i], max);	//return 0
	    }

	}
      if (DEBUG)
	printf ("\nEND PLAYER EARLY RETURN MIN:%d\n", max);
      return max;

    }

}

int
find_move (int player, int the_move, char board[])
{
  int games_states[BOARD_SIZE - 1];
  int j;
  char m[BOARD_SIZE];
  for (j = 0; j < BOARD_SIZE - 1; j++)	//set board states
    {
      games_states[j] = -3;
    }
  move (the_move, player, (char *) board);
  if (game_over ((char *) board))
    {
      int my_score = score ((char *) board);	//set score before erasing move
      unmove (the_move, (char *) board);
      return my_score;
    }
  get_valid_moves ((char *) board, (char *) m);
  int i;
  for (i = 0; i < strlen (m); i++)
    {
      if (m[i] != EMPTY)
	{
	  games_states[i] = find_move (!player, i, (char *) board);
	}

    }
  unmove (the_move, (char *) board);
  return minmax (player, games_states);


}


int
get_cpu_move (char board[])
{
  int i;
  char moves[BOARD_SIZE];
  int move_score = -1;		//worst case
  int temp_score = -1;		//best case
  int best_move = 99;		//best move for cpu
  get_valid_moves ((char *) board, (char *) moves);
  if (game_over ((char *) board))
    return -1;
  for (i = 0; i < strlen (moves); i++)
    {
      if (moves[i] != EMPTY)
	{
	  if (DEBUG)
	    printf ("\nBUILDING TREE FOR MOVE:%d %d\n", i, getpid ());
	  char temp_board[BOARD_SIZE];
	  strncpy (temp_board, board, sizeof (temp_board));
	  temp_score = find_move (PLAYER_X, i, (char *) temp_board);
	  if (temp_score > move_score)	//try to win or work for draw
	    {
	      if (DEBUG)
		if (DEBUG)
		  printf ("\nFUCK MOVE:%d %d %d\n", i, move_score,
			  temp_score);
	      move_score = temp_score;
	      best_move = i;
	    }
	  if (DEBUG)
	    printf ("\nBUILT TREE FOR MOVE:%d %d %d\n", i, move_score,
		    temp_score);
	}
      temp_score = -1;
    }
  return best_move;
}

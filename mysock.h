#include <sys/wait.h>
#include <signal.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>
#include <sys/types.h>
#include <arpa/inet.h>


/*Taken from stevens UNIX network programming*/
/*Shortens all the typecasts of pointer arguments*/
#define SA struct sockaddr
#define LISTENQ 1024

/*Miscellaneous Constants*/
#define MAXLINE 4096
#define UNIXSTR_PATH "/tmp/unix.str"
#define UNIXDG_PATH "/tmp/unix.dg"



/*Wrapper functions for socket operations*/
int Socket (int family, int type, int protocol);
int Bind (int sockfd, const struct sockaddr *my_addr, socklen_t addrlen);
int Listen (int sockfd, int backlog);
int Accept (int sockfd, struct sockaddr *addr, socklen_t * addrlen);
int Connect (int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen);

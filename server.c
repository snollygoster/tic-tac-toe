#include "mysock.h"
#include "tictoe.h"



#define MAX_PLAYERS 2
void player_o (int the_sig);
void wait_for_player_o ();


main (int argc, char *argv[])
{
  if (argc != 2)
    {
      char my_error[BUFSIZ];
      snprintf (my_error, sizeof (my_error), "Usage: %s <IP>", argv[0]);
      perror (my_error);
      exit (EXIT_FAILURE);
    }

  int listen_fd;
  int cpu_move;
  static struct sockaddr_in server_addr;	//change 2.1
  static struct sockaddr_in client_addr;	//change 2.1
  struct hostent *hp;		//needed for 2.3
  struct servent *sp;		//needed for 2.3
  int status;
  int connection_socket;
  pid_t pid;
  int c;
  int current_players = 0;
  char buf[MAXLINE];
  listen_fd = Socket (AF_INET, SOCK_STREAM, 0);	//change from 2.1
  server_addr.sin_family = AF_INET;
  if ((inet_pton (AF_INET, argv[1], &server_addr.sin_addr)) <= 0)
    {
      perror ("inet_pton");
      exit (EXIT_FAILURE);
    }
  server_addr.sin_port = htons (65201);
  Bind (listen_fd, (SA *) & server_addr, sizeof (struct sockaddr_in));
  Listen (listen_fd, LISTENQ);
  for (;;)
    {
      int client_length = sizeof (client_addr);
      connection_socket = Accept (listen_fd, (SA *) & client_addr, &client_length);	//blocks
      if ((pid = fork ()) == 0)
	{
	  int fd;
	  char board[BOARD_SIZE];
	  printf ("Connected mypid:%d parent:%d.......\n", getpid (),
		  getppid ());
	  close (listen_fd);	//decrement file reference count
	  do
	    {
	      if ((c = read (connection_socket, buf, MAXLINE)) > 0)
		{
		  strncpy ((char *) board, buf, c);	//copy board from client
	//	  print_board ((char *) board);
	//	  printf ("Valid Moves:");
	//	  print_moves ((char *) board);
		}
	      printf ("Valid Moves:");
	      print_moves ((char *) board);
	      cpu_move = get_cpu_move ((char *) board);
	      move (cpu_move, PLAYER_X, (char *) board);
	      write (connection_socket, board, sizeof (board));
	 //     print_board ((char *) board);
	      printf ("Valid Moves:");
	      print_moves ((char *) board);
	      print_board ((char *) board);
	    }
	  while (!game_over ((char *) board));
	  printf ("GAME WON BY PLAYER:%c\n", get_winner ((char *) board));

	  close (connection_socket);
	  exit (0);
	}
      wait (&status);			//without waiting I had many zombies
      close (connection_socket);	/*decrement file reference count, socket will be closed once child exits
					   if not done once child exits the socket will remain open because reference count
					   will be 1, the parent process */
      printf ("Closed out socket\n");

    }
}


void
player_o (int the_sig)
{
  printf ("PLAYER O HAS JOINED !!!!!\n");
}


void
wait_for_player_o ()
{
  struct sigaction new_action;
  sigset_t no_sigs, blocked_sigs, all_sigs;
  sigfillset (&all_sigs);	//turn on all bits
  sigemptyset (&no_sigs);	//turn off all bits
  sigemptyset (&blocked_sigs);
  new_action.sa_handler = player_o;
  new_action.sa_mask = all_sigs;
  new_action.sa_flags = 0;
  if (sigaction (SIGUSR1, &new_action, NULL) == -1)
    {
      perror ("SIGUSR1");
      exit (EXIT_FAILURE);
    }
  sigaddset (&blocked_sigs, SIGUSR1);
  sigprocmask (SIG_SETMASK, &blocked_sigs, NULL);
  for (;;)
    {
      printf ("Waiting for player o to join\n");
      sigsuspend (&no_sigs);	//wait
    }
}

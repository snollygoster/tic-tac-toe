#include "mysock.h"

int
Socket (int family, int type, int protocol)
{
  int n;
  if ((n = socket (family, type, protocol)) < 0)
    {
      perror ("Socket");
      exit (EXIT_FAILURE);
    }
  return n;
}

int
Bind (int sockfd, const struct sockaddr *my_addr, socklen_t addrlen)
{
  int n;
  if ((n = bind (sockfd, my_addr, addrlen)) < 0)
    {
      perror ("Bind");
      close (sockfd);
      exit (EXIT_FAILURE);
    }
  return n;
}

int
Listen (int sockfd, int backlog)
{
  int n;
  if ((n = listen (sockfd, backlog)) < 0)
    {
      perror ("Bind");
      exit (EXIT_FAILURE);
    }
  return n;
}

int
Accept (int sockfd, struct sockaddr *addr, socklen_t * addrlen)
{
  int n;
  if ((n = accept (sockfd, addr, addrlen)) < 0)
    {
      perror ("Accept");
      exit (EXIT_FAILURE);
    }
  return n;
}

int
Connect (int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen)
{
  int n;
  if ((n = connect (sockfd, serv_addr, addrlen)) < 0)
    {
      perror ("Connect");
      exit (EXIT_FAILURE);
    }
  return n;
}
